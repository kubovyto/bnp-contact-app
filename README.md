# Contact us Application

## Development

### How to build

* execute command *mvn package*

#### How to run
##### Local (standalone)
* To start application with embedded H2 database, use command:
```java
    java -jar server/target/contact-app.war 
```

* To start application with postgres database connection, use command and customize properties:
```java
    java -jar server/target/contact-app.war --spring.profiles.active=postgres --spring.datasource.url=jdbc:postgresql://localhost:5432/contactapp --spring.datasource.username=postgres --spring.datasource.password=postgres
```

After application start-up, open web browser on URL http://localhost:8080