/*
 * The code is property of OpenWise Solutions, s.r.o.
 * http://www.openwise.cz, Copyright (c) 2015
 */

package cz.kubovyto.bnp.contactapp.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Test application.
 *
 * @author Tomáš Kubový <a href="mailto:tomas.kubovy@openwise.cz">tomas.kubovy@openwise.cz</a>
 */
@SpringBootApplication
public class TestApplication {
    public static void main(String[] args) {
        SpringApplication.run(TestApplication.class, args);
    }

}
