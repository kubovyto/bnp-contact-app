package cz.kubovyto.bnp.contactapp.core.service.impl;

import java.util.List;

import cz.kubovyto.bnp.contactapp.core.TestApplication;
import cz.kubovyto.bnp.contactapp.core.domain.SaveRequestDO;
import cz.kubovyto.bnp.contactapp.core.repository.RequestRepository;
import cz.kubovyto.bnp.contactapp.core.repository.RequestTypeRepository;
import cz.kubovyto.bnp.contactapp.core.repository.entity.RequestEntity;
import cz.kubovyto.bnp.contactapp.core.service.RequestService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

/**
 * Tests of {@link RequestServiceImpl}.
 *
 * @author Tomáš Kubový <a href="mailto:tomas.kubovy@openwise.cz">tomas.kubovy@openwise.cz</a>
 */
@SpringBootTest(classes = TestApplication.class)
@ActiveProfiles("h2")
class RequestServiceImplTest {

    @Autowired
    private RequestService requestService;

    @Autowired
    private RequestTypeRepository requestTypeRepository;

    @Autowired
    private RequestRepository requestRepository;

    @Test
    @Transactional
    void save() {
        Assertions.assertThat(requestRepository.findAll()).isEmpty();

        SaveRequestDO toSave = new SaveRequestDO();
        toSave.setName("testName");
        toSave.setPolicyNumber("pol123");
        toSave.setSurname("testSurname");
        toSave.setTypeCode("DAMAGE_CASE");
        toSave.setText("test text");

        requestService.save(toSave);

        List<RequestEntity> all = requestRepository.findAll();
        Assertions.assertThat(all.size()).isOne();

        RequestEntity saved = all.get(0);
        Assertions.assertThat(saved.getName()).isEqualTo("testName");
        Assertions.assertThat(saved.getType().getCode()).isEqualTo("DAMAGE_CASE");
    }
}