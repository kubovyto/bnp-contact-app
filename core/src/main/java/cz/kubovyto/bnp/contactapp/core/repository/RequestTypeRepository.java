/*
 * The code is property of OpenWise Solutions, s.r.o.
 * http://www.openwise.cz, Copyright (c) 2015
 */

package cz.kubovyto.bnp.contactapp.core.repository;

import java.util.Optional;

import cz.kubovyto.bnp.contactapp.core.repository.entity.RequestTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for {@link RequestTypeEntity}.
 *
 * @author Tomáš Kubový <a href="mailto:tomas.kubovy@openwise.cz">tomas.kubovy@openwise.cz</a>
 */
@Repository
public interface RequestTypeRepository extends JpaRepository<RequestTypeEntity, Long> {

    /**
     * Find request type by code.
     *
     * @param code code of type
     * @return optionally found type
     */
    Optional<RequestTypeEntity> findByCode(String code);
}
