/*
 * The code is property of OpenWise Solutions, s.r.o.
 * http://www.openwise.cz, Copyright (c) 2015
 */

package cz.kubovyto.bnp.contactapp.core.repository.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Contact request entity.
 *
 * @author Tomáš Kubový <a href="mailto:tomas.kubovy@openwise.cz">tomas.kubovy@openwise.cz</a>
 */
@Data
@Entity(name = "request")
@NoArgsConstructor
public class RequestEntity {

    @Id
    @SequenceGenerator(name = "request_gen", sequenceName = "request_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "request_gen")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "request_type_id")
    private RequestTypeEntity type;

    @Column(name = "policy_number", length = 64, nullable = false)
    private String policyNumber;

    @Column(length = 64, nullable = false)
    private String name;

    @Column(length = 64, nullable = false)
    private String surname;

    @Column(length = 5000, nullable = false)
    private String text;
}
