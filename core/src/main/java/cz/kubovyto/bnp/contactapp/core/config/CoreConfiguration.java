/*
 * The code is property of OpenWise Solutions, s.r.o.
 * http://www.openwise.cz, Copyright (c) 2015
 */

package cz.kubovyto.bnp.contactapp.core.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Configuration of code module.
 *
 * @author Tomáš Kubový <a href="mailto:tomas.kubovy@openwise.cz">tomas.kubovy@openwise.cz</a>
 */
@Configuration
@EntityScan("cz.kubovyto.bnp.contactapp.core.repository.entity")
@EnableJpaRepositories(basePackages = "cz.kubovyto.bnp.contactapp.core.repository")
@ComponentScan("cz.kubovyto.bnp.contactapp.core")
public class CoreConfiguration {

}
