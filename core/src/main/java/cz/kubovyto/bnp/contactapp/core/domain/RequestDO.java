/*
 * The code is property of OpenWise Solutions, s.r.o.
 * http://www.openwise.cz, Copyright (c) 2015
 */

package cz.kubovyto.bnp.contactapp.core.domain;

import lombok.Data;

/**
 * Domain object of contact request.
 *
 * @author Tomáš Kubový <a href="mailto:tomas.kubovy@openwise.cz">tomas.kubovy@openwise.cz</a>
 */
@Data
public class RequestDO {
    private Long id;

    private String typeCode;
    private String typeName;

    private String policyNumber;
    private String name;
    private String surname;
    private String text;
}
