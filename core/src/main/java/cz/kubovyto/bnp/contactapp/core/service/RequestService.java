/*
 * The code is property of OpenWise Solutions, s.r.o.
 * http://www.openwise.cz, Copyright (c) 2015
 */

package cz.kubovyto.bnp.contactapp.core.service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import cz.kubovyto.bnp.contactapp.core.domain.RequestDO;
import cz.kubovyto.bnp.contactapp.core.domain.SaveRequestDO;

/**
 * Service to handle operations of contact requests.
 *
 * @author Tomáš Kubový <a href="mailto:tomas.kubovy@openwise.cz">tomas.kubovy@openwise.cz</a>
 */
public interface RequestService {

    /**
     * Save contact request to repository.
     *
     * @param requestDO request to be saved
     * @return saved request
     */
    @NotNull
    RequestDO save(@NotNull @Valid SaveRequestDO requestDO);

}
