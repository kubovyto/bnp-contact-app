/*
 * The code is property of OpenWise Solutions, s.r.o.
 * http://www.openwise.cz, Copyright (c) 2015
 */

package cz.kubovyto.bnp.contactapp.core.service.impl;

import javax.persistence.EntityNotFoundException;

import cz.kubovyto.bnp.contactapp.core.domain.RequestDO;
import cz.kubovyto.bnp.contactapp.core.domain.SaveRequestDO;
import cz.kubovyto.bnp.contactapp.core.mapper.RequestMapper;
import cz.kubovyto.bnp.contactapp.core.repository.RequestRepository;
import cz.kubovyto.bnp.contactapp.core.repository.RequestTypeRepository;
import cz.kubovyto.bnp.contactapp.core.repository.entity.RequestEntity;
import cz.kubovyto.bnp.contactapp.core.repository.entity.RequestTypeEntity;
import cz.kubovyto.bnp.contactapp.core.service.RequestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

/**
 * Service to handle operations of contact requests.
 *
 * @author Tomáš Kubový <a href="mailto:tomas.kubovy@openwise.cz">tomas.kubovy@openwise.cz</a>
 */
@Slf4j
@Service
@Validated
@Transactional(readOnly = true)
public class RequestServiceImpl implements RequestService {

    @Autowired
    private RequestRepository requestRepository;

    @Autowired
    private RequestTypeRepository requestTypeRepository;

    @Autowired
    private RequestMapper mapper;


    @Override
    @Transactional
    public RequestDO save(final SaveRequestDO requestDO) {
        LOG.info("Saving request {}", requestDO);

        final RequestTypeEntity requestTypeEntity = requestTypeRepository.findByCode(requestDO.getTypeCode())
                                                                         .orElseThrow(() -> new EntityNotFoundException("Can not find type by code " + requestDO.getTypeCode()));

        RequestEntity savedEntity = requestRepository.save(
            mapper.toRequestEntity(requestDO, requestTypeEntity));

        return mapper.toRequestDo(savedEntity);
    }
}
