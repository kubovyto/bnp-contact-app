/*
 * The code is property of OpenWise Solutions, s.r.o.
 * http://www.openwise.cz, Copyright (c) 2015
 */

package cz.kubovyto.bnp.contactapp.core.domain;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Domain object to save contact request.
 *
 * @author Tomáš Kubový <a href="mailto:tomas.kubovy@openwise.cz">tomas.kubovy@openwise.cz</a>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaveRequestDO {
    @NotNull
    private String typeCode;

    @NotEmpty
    @Size(max = 64)
    private String policyNumber;

    @NotEmpty
    @Size(max = 64)
    private String name;

    @NotEmpty
    @Size(max = 64)
    private String surname;

    @Size(max = 5000)
    private String text;
}
