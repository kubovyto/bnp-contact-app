/*
 * The code is property of OpenWise Solutions, s.r.o.
 * http://www.openwise.cz, Copyright (c) 2015
 */

package cz.kubovyto.bnp.contactapp.core.mapper;

import cz.kubovyto.bnp.contactapp.core.domain.RequestDO;
import cz.kubovyto.bnp.contactapp.core.domain.SaveRequestDO;
import cz.kubovyto.bnp.contactapp.core.repository.entity.RequestEntity;
import cz.kubovyto.bnp.contactapp.core.repository.entity.RequestTypeEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper between domain objects and entities.
 *
 * @author Tomáš Kubový <a href="mailto:tomas.kubovy@openwise.cz">tomas.kubovy@openwise.cz</a>
 */
@Mapper
public interface RequestMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "name", source = "requestDO.name")
    RequestEntity toRequestEntity(SaveRequestDO requestDO, final RequestTypeEntity type);

    @Mapping(target = "typeCode", source = "type.code")
    @Mapping(target = "typeName", source = "type.name")
    RequestDO toRequestDo(RequestEntity entity);

}
