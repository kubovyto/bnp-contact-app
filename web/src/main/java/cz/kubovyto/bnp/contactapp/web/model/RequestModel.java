/*
 * The code is property of OpenWise Solutions, s.r.o.
 * http://www.openwise.cz, Copyright (c) 2015
 */

package cz.kubovyto.bnp.contactapp.web.model;

import java.io.Serializable;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Model of contact us page.
 *
 * @author Tomáš Kubový <a href="mailto:tomas.kubovy@openwise.cz">tomas.kubovy@openwise.cz</a>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestModel implements Serializable {

    @NotNull
    private String typeCode;

    @NotEmpty
    @Size(max = 64)
    @Pattern(regexp = "^[a-zA-Z0-9]*$")
    private String policyNumber;

    @NotEmpty
    @Size(max = 64)
    @Pattern(regexp = "^\\p{Alpha}*$")
    private String name;

    @NotEmpty
    @Size(max = 64)
    @Pattern(regexp = "^\\p{Alpha}*$")
    private String surname;

    @Size(max = 5000)
    private String text;
}
