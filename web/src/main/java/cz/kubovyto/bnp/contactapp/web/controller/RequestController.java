/*
 * The code is property of OpenWise Solutions, s.r.o.
 * http://www.openwise.cz, Copyright (c) 2015
 */

package cz.kubovyto.bnp.contactapp.web.controller;

import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;

import cz.kubovyto.bnp.contactapp.core.repository.RequestTypeRepository;
import cz.kubovyto.bnp.contactapp.core.repository.entity.RequestTypeEntity;
import cz.kubovyto.bnp.contactapp.core.service.RequestService;
import cz.kubovyto.bnp.contactapp.web.WebConstants;
import cz.kubovyto.bnp.contactapp.web.mapper.RequestModelMapper;
import cz.kubovyto.bnp.contactapp.web.model.RequestModel;
import cz.kubovyto.bnp.contactapp.web.model.RequestTypeDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * Controller to handle contact request requests.
 *
 * @author Tomáš Kubový <a href="mailto:tomas.kubovy@openwise.cz">tomas.kubovy@openwise.cz</a>
 */
@Slf4j
@Controller
public class RequestController {
    @Autowired
    private RequestModelMapper mapper;

    @Autowired
    private RequestService requestService;

    @Autowired
    private RequestTypeRepository requestTypeRepository;

    public RequestController() {
        System.out.println();
    }

    /**
     * Prepare model and redirect to contact us page.
     *
     * @param model page model
     * @return contact us template name
     */
    @GetMapping(WebConstants.ResourcePath.CONTACT_US)
    public String contactUsPage(Model model) {
        model.addAttribute("data", new RequestModel());
        model.addAttribute("types", prepareTypes());
        return WebConstants.View.CONTACT_US;
    }

    /**
     * Save contact us request to repository.
     *
     * @param data          data to be saved
     * @param bindingResult binding errors
     * @param model         page model
     * @return template name
     */
    @PostMapping(WebConstants.ResourcePath.CONTACT_US)
    public String saveRequest(@Valid @ModelAttribute("data") RequestModel data,
                              BindingResult bindingResult,
                              Model model) {

        LOG.info("Saving contact us request {}", data);

        if (!bindingResult.hasErrors()) {
            requestService.save(mapper.toSaveRequestDo(data));

            model.addAttribute("data", new RequestModel());
            model.addAttribute("saved", true);
        }
        model.addAttribute("types", prepareTypes());

        return WebConstants.View.CONTACT_US;
    }

    /**
     * Prepare request types.
     *
     * @return list of request types.
     */
    private List<RequestTypeDTO> prepareTypes() {
        return requestTypeRepository.findAll(Sort.by(RequestTypeEntity.CODE_FIELD))
                                    .stream()
                                    .map(mapper::toRequestTypeDto)
                                    .collect(Collectors.toList());
    }

}
