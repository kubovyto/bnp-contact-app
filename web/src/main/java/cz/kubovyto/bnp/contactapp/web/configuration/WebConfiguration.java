/*
 * The code is property of OpenWise Solutions, s.r.o.
 * http://www.openwise.cz, Copyright 2018
 */
package cz.kubovyto.bnp.contactapp.web.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration for web module.
 *
 * @author Tomáš Kubový <a href="mailto:tomas.kubovy@openwise.cz">tomas.kubovy@openwise.cz</a>
 */
@Configuration
@ComponentScan("cz.kubovyto.bnp.contactapp.web")
public class WebConfiguration {

}
