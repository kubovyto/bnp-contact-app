/*
 * The code is property of OpenWise Solutions, s.r.o.
 * http://www.openwise.cz, Copyright (c) 2015
 */

package cz.kubovyto.bnp.contactapp.web.mapper;

import cz.kubovyto.bnp.contactapp.core.domain.SaveRequestDO;
import cz.kubovyto.bnp.contactapp.core.repository.entity.RequestTypeEntity;
import cz.kubovyto.bnp.contactapp.web.model.RequestModel;
import cz.kubovyto.bnp.contactapp.web.model.RequestTypeDTO;
import org.mapstruct.Mapper;

/**
 * Mapper between domain objects and DTO or page model objects.
 *
 * @author Tomáš Kubový <a href="mailto:tomas.kubovy@openwise.cz">tomas.kubovy@openwise.cz</a>
 */
@Mapper
public interface RequestModelMapper {

    SaveRequestDO toSaveRequestDo(RequestModel model);

    RequestTypeDTO toRequestTypeDto(RequestTypeEntity type);
}
