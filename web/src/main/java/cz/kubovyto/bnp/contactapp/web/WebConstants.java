/*
 * The code is property of OpenWise Solutions, s.r.o.
 * http://www.openwise.cz, Copyright 2018
 */
package cz.kubovyto.bnp.contactapp.web;

/**
 * Constants used be web module.
 */
public final class WebConstants {


    /**
     * View constants.
     */
    public static final class View {
        public static final String CONTACT_US = "contactus";


        private View() {
            // to avoid instantiate
        }

    }

    /**
     * Resource path constants.
     */
    public static final class ResourcePath {
        public static final String CONTACT_US = "/";

        private ResourcePath() {
            // to avoid instantiate
        }
    }

    private WebConstants() {
        // to avoid instantiate
    }
}
