/*
 * The code is property of OpenWise Solutions, s.r.o.
 * http://www.openwise.cz, Copyright (c) 2015
 */

package cz.kubovyto.bnp.contactapp.web.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Request types enumeration.
 *
 * @author Tomáš Kubový <a href="mailto:tomas.kubovy@openwise.cz">tomas.kubovy@openwise.cz</a>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestTypeDTO {
    private String code;
    private String name;
}
